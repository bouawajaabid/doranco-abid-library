package com.doranco.multitiers.sécurity;

import java.io.IOException;
import java.security.Key;



import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;

import org.apache.log4j.Logger;

import com.dornaco.multitiers.util.KeyGenerator;

import io.jsonwebtoken.Jwts;

public class SecurityCheckedInerceptor implements ContainerRequestFilter {

	KeyGenerator keyGenerator;
	Logger logger = Logger.getLogger( SecurityCheckedInerceptor.class);
	
	
		
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		// recupération des headers autorization de la requete
		
		String autorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		String token = autorizationHeader.substring("Bearer".length()).trim();
		
 if (autorizationHeader==null || autorizationHeader.startsWith("Bearer ")) {
	 logger.error("Authorization header valide");
	 
	 throw new NotAuthorizedException("L'entête authorization devrait être fournie");
 }
 
// Key key = keyGenerator.generateKey();
// Jwts.parser().setSigningKey(key).parseClaimsJws(token);
// logger.info("token valide : "+token);
		
	
	
	try {
		Key key = KeyGenerator.getINSTANCE().getKey();
		System.out.println("=====> la cle que l'on a pour controle : " + key.getFormat());
		logger.info("[filter]  ----- > " + key.getFormat() +"/" + key.toString());
		Jwts.parser().setSigningKey(key).parseClaimsJws(token);

	} catch (Exception e) {
		logger.error("[filter]erreur lors de la validation de la key(token)", e);
		// ne doit jamais arriver, car on ne passera pas si le token n'a pas été generé
	}

	logger.debug("[filter]valid token: " + token);
}

	

}
