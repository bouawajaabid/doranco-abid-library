package com.doranco.multitiers.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.doranco.multitiers.entity.Order;
import com.doranco.multitiers.vm.OrderVM;

@Mapper(uses=OrderLineMapper.class)
public interface OrderMapper {
	
	 OrderMapper INSTANCE = Mappers.getMapper( OrderMapper.class ); 
	
	 
	 
		@Mapping(source = "user.id", target = "idUser")
		OrderVM entityToVm(Order entity);

//		@Mapping(source = "book.id", target = "idBook")
//		OrderLineVM entityToVm(OrderLine entity);
		
		
		List<OrderVM> entitiesToVMs(List<Order> entities);
	 
 

}
