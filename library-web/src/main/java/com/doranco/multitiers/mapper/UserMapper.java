package com.doranco.multitiers.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.vm.UserVM;
import com.dornaco.multitiers.util.Page;

@Mapper(uses= {NoteMapper.class,OrderMapper.class,ViewingMapper.class})
public interface UserMapper {
	
	
	 UserMapper INSTANCE = Mappers.getMapper( UserMapper.class ); 
	 
	 
	 @Mapping(target ="password", ignore=true)
	 UserVM EntityToVm(User entity);
	 
	 List<UserVM> entitiesToVMs (List<User> user) ;
	 
	 Page<UserVM> entitiesPageToVMsPage(Page<User> page);
	 
 @Mapping(target = "id" ,ignore = true)
	User VmToEntities(UserVM user);
	 

}
