package com.doranco.multitiers.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.vm.NoteVM;

@Mapper
public interface NoteMapper {

	NoteMapper INSTANCE = Mappers.getMapper(NoteMapper.class);

	@Mappings({ @Mapping(source = "book.id", target = "idBook"), @Mapping(source = "user.id", target = "idUser") })

	NoteVM entityToVm(Note entity);

	List<NoteVM> entitiesToVMs(List<Note> entities);

	Note VmToEntities(NoteVM note);

}
