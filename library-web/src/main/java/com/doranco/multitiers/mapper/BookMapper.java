package com.doranco.multitiers.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.vm.BookVM;
import com.dornaco.multitiers.util.Page;

@Mapper(uses = { NoteMapper.class })
public interface BookMapper {

	BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

	BookVM EntityToVm(Book entity);

	List<BookVM> entitiesToVMs(List<Book> entities);

	Page<BookVM> entitiesPageToVMsPage(Page<Book> page);

	Book VmToEntities(BookVM book);

}
