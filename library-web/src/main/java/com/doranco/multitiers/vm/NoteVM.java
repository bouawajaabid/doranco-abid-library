package com.doranco.multitiers.vm;

import java.io.Serializable;

public class NoteVM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8310905909991538631L;
	private int value;
	private String Comment;
	private long idBook;
	private long idUser;

	public NoteVM() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getComment() {
		return Comment;
	}

	public void setComment(String comment) {
		Comment = comment;
	}

	public void setIdBook(long idBook) {
		this.idBook = idBook;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	@Override
	public String toString() {
		return "NoteVM [value=" + value + ", Comment=" + Comment + ", idBook=" + idBook + ", idUser=" + idUser + "]";
	}

}
