package com.doranco.multitiers.vm;

import java.io.Serializable;

public class CredentialsVM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5851377746854504906L;
	private String userName;
	private String password;

	public CredentialsVM() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
}
