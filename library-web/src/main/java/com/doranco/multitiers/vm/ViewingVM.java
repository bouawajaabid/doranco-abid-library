package com.doranco.multitiers.vm;

import java.io.Serializable;
import java.util.Date;

public class ViewingVM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7496359172598579972L;
	private long idBook;
	private long duration;
	private Date startDate;

	public ViewingVM() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getIdBook() {
		return idBook;
	}

	public void setIdBook(long idBook) {
		this.idBook = idBook;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

}
