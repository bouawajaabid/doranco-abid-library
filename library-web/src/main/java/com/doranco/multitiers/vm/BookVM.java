package com.doranco.multitiers.vm;

import java.io.Serializable;
import java.util.List;

public class BookVM implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3200406382685587989L;
	private long id;
	private String title;
	private String ISBN;
	
	
	private List<NoteVM> note;

	public BookVM() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public List<NoteVM> getNote() {
		return note;
	}

	public void setNote(List<NoteVM> note) {
		this.note = note;
	}

}
