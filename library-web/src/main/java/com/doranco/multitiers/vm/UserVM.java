package com.doranco.multitiers.vm;

import java.io.Serializable;
import java.util.List;

public class UserVM implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2951331454379337090L;
	private long id;
	private String firstName;
	private String lastName;
	private boolean isAdmin = false;
	private String userName;
	private String password;
	



	private List<NoteVM> note;
	
	private List<OrderVM> order;
	
	private List<ViewingVM> viewing;
	 
	 
	 
	 
	 
	public UserVM() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<NoteVM> getNote() {
		return note;
	}

	public void setNote(List<NoteVM> note) {
		this.note = note;
	}

	public List<OrderVM> getOrder() {
		return order;
	}


	public void setOrder(List<OrderVM> order) {
		this.order = order;
	}


	public List<ViewingVM> getViewing() {
		return viewing;
	}


	public void setViewing(List<ViewingVM> viewing) {
		this.viewing = viewing;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
 
	
}
