package com.doranco.multitiers.vm;

import java.io.Serializable;
import java.util.List;

import com.doranco.multitiers.entity.OrderLine;

public class OrderVM  implements Serializable{
	
	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private long idUser;
	
	private List<OrderLine> orderLineVM;
	
	
	
	
	public OrderVM() {
		super();
		// TODO Auto-generated constructor stub
	}


  public long getId() {
		return id;
	}


    public void setId(long id) {
		this.id = id;
	}


	public List<OrderLine> getOrderLineVM() {
		return orderLineVM;
	}

	public void setOrderLineVM(List<OrderLine> orderLineVM) {
		this.orderLineVM = orderLineVM;
	}


	public long getIdUser() {
		return idUser;
	}




	
	

}
