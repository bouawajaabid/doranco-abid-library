package com.doranco.multitiers.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.Panier;
import com.doranco.multitiers.exceptions.LibraryException;

/**
 * Servlet implementation class Home
 */
//@WebServlet("/")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Panier panier;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Home() {
		super();
		// TODO Auto-generated constructor stub
		try {

			panier = (Panier) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/PanierBean");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		
			// Ajouter du Livre

           try {
			panier.addBook((String) request.getParameter("addbook"));
		} catch (LibraryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
           try {
			panier.removeBook((String)request.getParameter("removeBook"));
		} catch (LibraryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
System.out.println((String) request.getParameter("addbook"));
			// Supprime du livre
           try {
    	   //panier.removeBook((String) request.getAttribute("removeBook"));
		List<String> books = panier.getBooks();
		request.setAttribute("books", books);

			this.getServletContext().getRequestDispatcher("/jsp/index.jsp").forward(request, response);
		} catch (LibraryException e) {

			System.out.println(e.getMessage());
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
