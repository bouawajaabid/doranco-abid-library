package com.doranco.multitiers.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.doranco.multitiers.ejb.interfaces.INote;
import com.doranco.multitiers.mapper.NoteMapper;
import com.doranco.multitiers.vm.NoteVM;

@Path("/note")
@Produces(MediaType.APPLICATION_JSON)
public class NoteResource {

	INote iNote;
	Logger logger = Logger.getLogger(UserResources.class);
	NoteMapper noteMapper = NoteMapper.INSTANCE;
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public NoteVM createNote(NoteVM note) {
	
		try {
			
		iNote.createNote (noteMapper.VmToEntities(note));
		} catch (Exception e) {
			logger.error("Impossible de recupére les notes mit par les clients",e);
			// TODO: handle exception
		}

		
		return note;
		
	}
	
	
	
	
}
