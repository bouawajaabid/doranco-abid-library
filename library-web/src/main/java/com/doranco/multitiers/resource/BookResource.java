package com.doranco.multitiers.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.doranco.multitiers.ejb.interfaces.IBook;
import com.doranco.multitiers.mapper.BookMapper;
import com.doranco.multitiers.vm.BookVM;
import com.dornaco.multitiers.util.Page;

@Path("/book")
@Produces(MediaType.APPLICATION_JSON)
public class BookResource {

	IBook iBook;
	Logger logger = Logger.getLogger(UserResources.class);
	BookMapper bookMapper = BookMapper.INSTANCE;
	
	@GET

	public Page<BookVM> getBook(@QueryParam("pageSize") int pageSize, @QueryParam("pageNumber") int pageNumber) {
		Page<BookVM> bookVMs = null;
		try {
			bookVMs = bookMapper.entitiesPageToVMsPage(iBook.find(pageSize, pageNumber));

		} catch (Exception e) {
			logger.error("Impossible de ce referencie sur la liste du livre", e);
		}

		return bookVMs;
	}
	

	
	
	
	@Path("{id}")
	@GET
	public BookVM getBook(@PathParam("id" )long id) {
		BookVM bookVM =null;
		logger.debug("Trying to get a book from its id =="+id);
		try {
			bookVM = bookMapper.EntityToVm(iBook.getBook(id));
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("Impossible de récupére un book", e);
		}
		return bookVM;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public BookVM createBook(BookVM book) {
	
		try {
			
		iBook.createBook (bookMapper.VmToEntities(book));
		} catch (Exception e) {
			logger.error("Impossible de recupére les paramétre mit par les clients",e);
			// TODO: handle exception
		}

		
		return book;
		
	}
}
