package com.doranco.multitiers.resource.execption.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import org.apache.log4j.Logger;

import com.doranco.multitiers.exceptions.EntityNotFoundException;

public class EntityNotFoundMapper implements ExceptionMapper<EntityNotFoundException> {

	Logger logger= Logger.getLogger(EntityNotFoundException.class);
	
	@Override
	public Response toResponse(EntityNotFoundException exception) {
		logger.error("Impossible de récupérer l'entite avec l'id passe en param", exception);
		
		return Response.status(Status.NOT_FOUND).header("Server Message", "Impossible de retrouver l'élément recherche").build();
	}
	
}
