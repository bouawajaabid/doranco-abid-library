package com.doranco.multitiers.resource;

import java.net.URI;

import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.IUser;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.exceptions.UserNotFoundException;
import com.doranco.multitiers.mapper.UserMapper;
import com.doranco.multitiers.sécurity.SecurityChecked;
import com.doranco.multitiers.vm.CredentialsVM;
import com.doranco.multitiers.vm.UserVM;
import com.dornaco.multitiers.util.Page;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
public class UserResources {

	IUser iUser;

	@Context
	UriInfo uriInfo;

	Logger logger = Logger.getLogger(UserResources.class);
	UserMapper userMapper = UserMapper.INSTANCE;

	public UserResources() throws LibraryException {
		super();
		try {
			iUser = (IUser) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/UserBean");
			//
		} catch (NamingException e) {
			logger.error("Impossible ");
			throw new LibraryException();
		}
	}

	@POST
	public User suscribe(User user) {

		try {
			user = this.iUser.suscribe(user);
		} catch (LibraryException e) {
			logger.error("Impossible la resource User Resource");
		}
		return user;
	}

	@Path("{id}")
	@GET
	@SecurityChecked
	public Response getUser(@PathParam("id") long id) throws LibraryException {
		UserVM userVM = null;
		logger.debug("Trying to get a user from its id ==" + id);

		userVM = userMapper.EntityToVm(iUser.getUser(id));

		return Response.ok().entity(userVM).build();
	}

	@GET

	public Response  getUser(@QueryParam("pageSize") int pageSize,
			@QueryParam("pageNumber") int pageNumber) throws LibraryException {
		Page<UserVM> userVMs = null;
			userVMs = userMapper.entitiesPageToVMsPage(iUser.find(pageSize, pageNumber));

		return Response.ok().entity(userVMs).build();
	}

// Methode 
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createUser(UserVM user) throws LibraryException {

		User user1 = iUser.suscribe(userMapper.VmToEntities(user));
		user = userMapper.EntityToVm(user1);
		URI uri = UriBuilder.fromResource(user.getClass()).build();
		return Response.created(uri).entity(user).header("server message ", "Utilisateur ").build();

	}

	// Methode qui nous permetont de sécurise l'entre d'utilisateur avec le token
	@POST
	@Path("login")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response authenticate(CredentialsVM cred) throws LibraryException {

		
			String token = iUser.connect(cred.getUserName(), cred.getPassword(), uriInfo.getAbsolutePath().toString());

			return Response.ok().header(HttpHeaders.AUTHORIZATION, "Bearer " + token).build();

		

			
		

	}

}
