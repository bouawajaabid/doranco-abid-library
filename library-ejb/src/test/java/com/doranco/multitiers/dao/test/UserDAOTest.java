package com.doranco.multitiers.dao.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.EJBException;

import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.doranco.multitiers.dao.GenericDAO;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.entity.User;



@RunWith(Arquillian.class)
public class UserDAOTest {

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class) .addClasses(UserDAO.class, GenericDAO.class)
		;
	}
	@EJB
	private UserDAO dao;
	
	Logger log = Logger.getLogger(UserDAOTest.class);
	
	
	@Test
	@InSequence(1)
	public final void shouldCreateAUser() {
	
		try {
			for (int i =0 ; i<10; i++) {
				User user = new User ();
				user.setFirstName("FirstName "+i);
				user.setLastName("LasteName " +i );
				user.setPassword("PassWord "+ i);
				user.setUserName("UserName " +i);
				dao.create(user);
			}
			
			
			
		} catch (Exception e) {
			log.error("ERROR = La creation est mal fait",e);
			fail("Ne devrait pas retourne d'exception");
		}
		
		
	}

	//@Test
	@InSequence(2)
	public final void shouldFineAUserWithAGoodPassWord() {
		try {
			User connectedUser = dao.findByUserNameAndPassWord("Abid", "abidbouawaja");
			assertNotNull(connectedUser);
			
		} catch (Exception e) {
//			log.error("ERROR = ",e);
			fail("Ne devrait pas retourne d'exception lors de la récupération d'un user avec de bon identifiant");
			
		}
	
	}
	//@Test
	@InSequence(3)
	public final void shouldNotFineAUserWithAGoodPassWord() {
		User user = null;
		try {
			user =	dao.findByUserNameAndPassWord("Abid", "abiidbouawaja");
			
		} catch (EJBException nre) {
		
		}
		assertNull(user);
	}
	@Test
	public final void shouldFineAllUser() throws Exception {
		List<User> lUser = new ArrayList<>();
		try {
				lUser = (List<User>) dao.findAllUser();
		} catch (EJBException nre) {
			
		}
		log.debug("Les users trouvé sont: " + lUser);
	}
	
	


//    @Test
//    public void shouldMapUserToUserVM() {
//        //given
//        User user = new User();
//        
//        user.setFirstName("Abid ");
//		user.setLastname("BOUAWAJA "  );
//		user.setPassword("bouawaja");
//		user.setUserName("abidbouawaja " );
//		dao.create(user);
//        //when
//       UserVM uservm = UserMapper.INSTANCE.UserToUserVM( user );
//     
//        //then
//        assertThat( uservm ).isNotNull();
//        assertThat(uservm.getFirstName() ).isEqualTo( "Abid" );
//        assertThat( uservm.getLastName() ).isEqualTo("BOUAWAJA" );
//        assertThat(uservm.getidUser() ).isEqualTo( "26" );
//    }


		

	//@Test
	public final void testUpdate() {
	}

	//@Test
	public final void testFind() {
	}

}
