package com.doranco.multitiers.dao.test;

//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.assertNull;
//import static org.junit.Assert.fail;
//
//import javax.ejb.EJB;
//
//import org.apache.log4j.Logger;
//import org.jboss.arquillian.container.test.api.Deployment;
//import org.jboss.arquillian.junit.Arquillian;
//import org.jboss.arquillian.junit.InSequence;
//import org.jboss.shrinkwrap.api.ShrinkWrap;
//import org.jboss.shrinkwrap.api.spec.JavaArchive;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import com.doranco.multitiers.dao.BookDAO;
////import com.doranco.multitiers.dao.CartDAO;
//import com.doranco.multitiers.entity.Book;
//import com.doranco.multitiers.entity.Cart;

//@RunWith(Arquillian.class)
////public class CartDAOTest {
//	@Deployment
//	public static JavaArchive createDeployment() {
//		return ShrinkWrap.create(JavaArchive.class, "test.jar").addClass(CartDAO.class)
//		// .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
//		;
//
//	}

//	Logger logger = Logger.getLogger(CartDAO.class);
//
//	@EJB
//	CartDAO dao;
//
//	@Test
//	@InSequence(1)
//	public void shouldCreateCarts() {
//		try {
//			// creation de champ à la base de donnée
//
//			for (int i = 1; i <= 20; i++) {
//				Cart cart = new Cart();
//				cart.setName("L'Actualité de  jours " + i);
//				cart = dao.create(cart);
//			}
//
//		} catch (Exception e) {
//
//			fail("Ne devrait pas retournes d'exception");
//		}
//	}
//
//	@Test
//	@InSequence(2)
//	public void shouldFindItem() {
//		Cart foundCart = dao.find(Cart.class, 5);
//		assertNotNull(foundCart);
//	}
//
////	
//
//	@Test
//	@InSequence(3)
//	public void shouldUpdateDataBase() {
//		Cart foundCart = dao.find(Cart.class, 5);
//		if (foundCart != null) {
//			logger.debug("Le panier d'ID 5 " + foundCart + " est en mise à jour");
//			foundCart.setName("lecture de l'année");
//
//			dao.update(foundCart);
//			logger.debug("Le pannier d'ID 5 est " + foundCart);
//		}
//		Cart updatedCart = dao.find(Cart.class, 5);
//		assertEquals(updatedCart.getName(), "lecture de l'année");
//	}
//
//	@Test
//	@InSequence(4)
//	public void shouldDeleteACart() {
//		Cart foundCart = dao.find(Cart.class, 15);
//		logger.debug("Le panier d'id 15 est : " + foundCart);
//
//		dao.delete(foundCart);
//		foundCart = dao.find(Cart.class, 15);
//		assertNull(foundCart);
//	}
//
//}
