package com.doranco.multitiers.dao.test;



import static org.junit.Assert.fail;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.dao.GenericDAO;
import com.doranco.multitiers.entity.Book;

@RunWith(Arquillian.class)
public class BookDAOTest {
	
	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class) .addClasses(BookDAO.class, GenericDAO.class)
		;
	}
	
	@EJB
	BookDAO bookDao; 
	Logger logger =Logger.getLogger(BookDAOTest.class);
	
	@Test
	public final void shouldBeCreateBook () {
		
		try {
			
			for (int i = 0; i<5;i++) {
				
				Book book = new Book();
				book.setISBN("iSBN " +i);
				book.setTitle("title de book "+i);
				bookDao.create(book);
			}
			
		} catch (Exception e) {
			logger.error("Le remplissage de book est mal fait ",e);
			fail("Ne devrait pas retourne d'exception");
		}
	}

}
