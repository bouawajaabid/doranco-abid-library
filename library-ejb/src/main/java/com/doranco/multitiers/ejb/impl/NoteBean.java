package com.doranco.multitiers.ejb.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.doranco.multitiers.dao.NoteDAO;
import com.doranco.multitiers.ejb.interfaces.INote;
import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.exceptions.LibraryException;
@Stateless
public class NoteBean implements INote {

	Logger logger = Logger.getLogger(BookBean.class);
	
	@EJB
	NoteDAO noteDAO;
	@Override
	public void createNote(Note note) throws LibraryException {
		try {
			
			noteDAO.create(note);
			logger.debug("L'avis est bien crée");
			
	throw new LibraryException();
		} catch (Exception e) {
			logger.error("Attention l'avis n'a pas été crée",e);
			
			throw new LibraryException();
		}
		
	}

}
