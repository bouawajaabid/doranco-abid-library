package com.doranco.multitiers.ejb.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;

import com.doranco.multitiers.ejb.interfaces.Panier;
import com.doranco.multitiers.exceptions.LibraryException;

@Stateless
public class PanierBean implements Panier {

	List<String> books = new ArrayList<String>(Arrays.asList("Twighlight", "Harry potter", "Vampier"));

	@Override
	public void addBook(String book) throws LibraryException {
		// parcours la liste des livres
		boolean a= false;
		for (int i = 0; i < books.size() && a == false; i++) {
			// Vérfier que le livre n'existe pas dans la liste
		if(this.books.get(i).equals(book)) {
			this.books.add(book);
		}
			
		}
		//this.books.add(book);
	}

	@Override
	public void removeBook(String book) throws LibraryException {
		// parcours la liste des livres
		boolean r = true;
		for (int i = 0; i < books.size()&& r == true; i++) {
			if (this.books.get(i).equals(book)) {
				this.books.remove(book);	
			}
			// Vérfier que le livre n'existe pas dans la liste
			
			
		}

	}

	@Override
	public List<String> getBooks() throws LibraryException {
		List<String> result = null;
		try {
			// va aller recuperer les livres de la BD
			result = books;

		} catch (Exception e) {
			// Loguer la stack strace dans le fichier de logs

			e.printStackTrace();
			throw new LibraryException("Problème survenu lors de recupération des livres");

		}

		return result;
	}

}
