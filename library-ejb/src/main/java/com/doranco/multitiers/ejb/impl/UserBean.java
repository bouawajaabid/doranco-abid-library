package com.doranco.multitiers.ejb.impl;

import java.io.IOException;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.ejb.TransactionRolledbackLocalException;
import javax.persistence.NoResultException;
import javax.transaction.TransactionRolledbackException;

import org.apache.log4j.Logger;

import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.ejb.interfaces.IUser;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.EntityNotFoundException;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.exceptions.UserNotFoundException;
import com.dornaco.multitiers.util.KeyGenerator;
import com.dornaco.multitiers.util.Page;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Stateless
public class UserBean implements IUser {
	Logger logger = Logger.getLogger(UserBean.class);

	@EJB

	UserDAO userDAO;

	private KeyGenerator keyGenerator;

	public UserBean() throws UnrecoverableKeyException, KeyStoreException, CertificateException, IOException  {
		// pour générer un clée de façon aléatoire
		try {
			Key key = KeyGenerator.getINSTANCE().getKey();

		} catch (NoSuchAlgorithmException e) {
			logger.error("Impossible de démarrer UserBean", e);
			throw new IOException();
		}

	}

	// La date d'envoi de requete
	private Date toDate(LocalDateTime localDateTime) {

		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

	}

	@Override
	public User suscribe(User user) throws LibraryException {

		try {
			userDAO.create(user);
		} catch (Exception e) {

			logger.error("Problème due lors de l'inscription de l'utilisateur", e);
			throw new LibraryException();
		}
		return user;

	}

	private String issueToken(String login, String uriInfo) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException  {

		logger.debug("uriINfo==> " + uriInfo);

		Key key = KeyGenerator.getINSTANCE().getKey();

		String jwtToken = Jwts.builder().setSubject(login).setIssuer(uriInfo)

				.setIssuedAt(new Date()).setExpiration(toDate(LocalDateTime.now().plusMinutes(15L)))

				.signWith(SignatureAlgorithm.HS512, key).compact();

		logger.info("#### generating token for a key : " + jwtToken + " - " + key);

		return jwtToken;
	}

	@Override
	public String connect(String userName, String password, String uri) throws LibraryException {

		String token = null;
		try {
			
			userDAO.findByUserNameAndPassWord(userName, password);
			token = issueToken(userName, uri);
 
			
		} catch (EJBTransactionRolledbackException nre) {

			UserNotFoundException une = new UserNotFoundException(nre);
			logger.error("Impossible de trouver un tel utilisateur", une);
			throw une;
		} catch (Exception e) {
			logger.error("Problème lors de la connexion d'un ", e);

			throw new LibraryException();
		}

		return token;
	}

	@Override
	public List<User> findAllUser() throws LibraryException {
		List<User> lUser = new ArrayList<>();
		try {
			lUser = (List<User>) userDAO.findAllUser();
		} catch (NoResultException nre) {

			logger.error("La liste d'utilisateur est vide", nre);

			throw new LibraryException();
		} catch (Exception e) {
			logger.error("Problème lors de la connexion sur la liste d'utilisateur", e);

			throw new LibraryException();
		}

		return lUser;
	}

	@Override

	public User getUser(long id) throws LibraryException {
		User user = null;
		try {
			user = userDAO.find(id);
			if (user == null)
				throw new EntityNotFoundException();			
		} catch (Exception e) {
			logger.error("Problème lors de la récupération d'un user à partir de son id", e);
			throw new LibraryException();
		}
		return user;
	}

	@Override
	public Page<User> find(int pageSize, int pageNumber) throws LibraryException {
		// TODO Auto-generated method stub
		Page<User> lUser = null;
		try {
			lUser = (Page<User>) userDAO.find(pageSize, pageNumber);
		} catch (NoResultException nre) {

			logger.error("La liste d'utilisateur est vide", nre);

			throw new LibraryException();
		} catch (Exception e) {
			logger.error("Problème lors de la connexion sur la liste d'utilisateur", e);

			throw new LibraryException();
		}

		return lUser;

	}

	@Override
	public void createUser(User user) throws LibraryException {

		try {
			userDAO.create(user);

			logger.debug("La creation d'utilisateur est fait");
			throw new LibraryException();
		} catch (Exception e) {
			logger.error("la creation de nouveau utilisateur ça fonction pas", e);
			throw new LibraryException();
		}

	}

}
