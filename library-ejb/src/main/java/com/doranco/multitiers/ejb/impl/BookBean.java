package com.doranco.multitiers.ejb.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;

import org.apache.log4j.Logger;

import com.doranco.multitiers.dao.BookDAO;
import com.doranco.multitiers.ejb.interfaces.IBook;
import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.exceptions.LibraryException;
import com.dornaco.multitiers.util.Page;

@Stateless
public class BookBean implements IBook {

	Logger logger = Logger.getLogger(BookBean.class);
	@EJB

	BookDAO bookDAO;

	@SuppressWarnings("unchecked")
	@Override
	public List<Book> findBook(String title) throws LibraryException {
		List<Book> lBook = null;
		try {
			lBook = (List<Book>) bookDAO.findByBookName(title);
		} catch (NoResultException nre) {

			logger.error("La liste de book est vide", nre);

			throw new LibraryException();
		} catch (Exception e) {
			logger.error("Problème lors de la recherche des livre", e);

			throw new LibraryException();
		}

		return lBook;
	}

	@Override
	public Page<Book> find(int pageSize, int pageNumber) throws LibraryException {
		// TODO Auto-generated method stub
		Page<Book> lBook = null;
		try {
			lBook = (Page<Book>) bookDAO.find(pageSize, pageNumber);
		} catch (NoResultException nre) {

			logger.error("La liste d'utilisateur est vide", nre);

			throw new LibraryException();
		} catch (Exception e) {
			logger.error("Problème lors de la connexion sur la liste d'utilisateur", e);

			throw new LibraryException();
		}

		return lBook;
	}

	@Override
	public Book getBook(long id) throws LibraryException {
		Book book = null;
		try {
			book = bookDAO.find(id);
		} catch (Exception e) {
			logger.error("Problème lors de la récupération d'un user à partir de son id", e);
			throw new LibraryException();
		}
		return book;
	}

	@Override
	public void createBook(Book book) throws LibraryException {

		try {
			bookDAO.create(book);
			logger.debug("La création est faite");
			throw new LibraryException();
		} catch (Exception e) {

			logger.error("Impossible de créer un nouveau book", e);
			throw new LibraryException();
		}

	}
}
