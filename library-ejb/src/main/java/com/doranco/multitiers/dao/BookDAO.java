package com.doranco.multitiers.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Hibernate;

import com.doranco.multitiers.entity.Book;
import com.dornaco.multitiers.util.Page;

@Stateless
@LocalBean
public class BookDAO extends GenericDAO<Book> {

	String jpql;

	public Book findByBookName(String givenBookName) throws NoResultException {
		Book book;
		jpql = "SELECT b FROM Book b WHERE b.title=:title ";

		book = (Book) em.createQuery(jpql).setParameter("title", givenBookName);

		return book;
	}

	public Book find(long id) {
		Book book = null;

		book = em.find(Book.class, id);
		hibernateInitialize(book);
		return book;
	}

	public Page<Book> find(int pageSize, int pageNumber) {

		Page<Book> page = new Page<Book>();

		jpql = "SELECT b FROM Book b";

		Query query = em.createQuery(jpql);

		query.setMaxResults(pageSize);
		query.setFirstResult(pageNumber * pageSize + 1);

		// récuperation d'une page des Livres

		List<Book> pageContent = query.getResultList();

		// récuperation de nbre total des books dans BD

		long totalCount = (long) em.createQuery("SELECT count(b.id) FROM Book b").getSingleResult();

		for (Book book : pageContent) {

			hibernateInitialize(book);
		}
		page.setPageNumber(pageNumber);
		page.setPageSize(pageSize);
		page.setTotalCount(totalCount);
		page.setContent(pageContent);
		return page;

	}

	private void hibernateInitialize(Book book) {
		Hibernate.initialize(book.getNotes());
		Hibernate.initialize(book.getViews());

	}

}
