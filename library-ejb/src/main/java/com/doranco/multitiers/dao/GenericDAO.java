package com.doranco.multitiers.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;


public class GenericDAO<T> {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("library");
	EntityManager em = emf.createEntityManager();
	public EntityTransaction transaction = em.getTransaction();

	public T create(T toBeCreated) throws Exception   {
		transaction.begin();
		em.persist(toBeCreated);
		em.flush();
		transaction.commit();
		return toBeCreated;

	}
	public void  delete(T toBeCreated) throws Exception {
		transaction.begin();
		em.remove(toBeCreated);
		transaction.commit();
		
	}
	public T update(T toBeUpdated) throws Exception{

		transaction.begin();
		em.merge(toBeUpdated);
		transaction.commit();
		return toBeUpdated;
	}
	public T find(Class clazz, Integer primaryKey) throws Exception{
		return (T) em.find(clazz, primaryKey);
	}
// public List<T> findAll(Class clazz){
//	 
// }
}
