package com.doranco.multitiers.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Hibernate;

import com.doranco.multitiers.entity.User;
import com.dornaco.multitiers.util.Page;

@Stateless
@LocalBean
public class UserDAO extends GenericDAO<User> {
	String jpql = null;

	public User findByUserNameAndPassWord(String givenUserName, String givenPassWord) throws NoResultException {
		User user;
		String jpql = "SELECT u FROM User u WHERE u.userName=:userName AND u.password=:password";

		user = (User) em.createQuery(jpql).setParameter("userName", givenUserName)
				.setParameter("password", givenPassWord).getSingleResult();
		hibernateInitialize(user);
		return user;
	}

	@SuppressWarnings("unchecked")
	public List<User> findAllUser() throws Exception {
		List<User> users;

		String jpql = "SELECT u FROM User u ORDER BY id DESC";
		users = em.createQuery(jpql).getResultList();
		for (User user : users) {
			hibernateInitialize(user);
		}
		return users;
	}

	public User find(long id) {
		User user = null;

		user = em.find(User.class, id);
		hibernateInitialize(user);
		return user;
	}

	public Page<User> find(int pageSize, int pageNumber) {

		Page<User> page = new Page<User>();

		jpql = "SELECT u FROM User u ";

		Query query = em.createQuery(jpql);

		query.setMaxResults(pageSize);
		query.setFirstResult(pageNumber * pageSize + 1);

		// récuperation d'une page d'utilisateur
		List<User> pageContent = query.getResultList();

		// récuperation de nbre total des users
		long totalCount = (long) em.createQuery("SELECT count(u.id) FROM User u").getSingleResult();

		for (User user : pageContent) {

			hibernateInitialize(user);
		}
		page.setPageNumber(pageNumber);
		page.setPageSize(pageSize);
		page.setTotalCount(totalCount);
		page.setContent(pageContent);
		return page;

	}

	private void hibernateInitialize(User user) {
		Hibernate.initialize(user.getNotes());
		Hibernate.initialize(user.getViews());
		Hibernate.initialize(user.getOrder());
	}

}
