package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.mail.Message;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "l_user")

public class User extends Identifiant implements Serializable {

	/**
	 * 
	 */
	public User() {

	}
	private static final long serialVersionUID = 1L;
	private String firstName ;
	@NotNull
	private String lastName;
	
	@NotNull
	private String userName;
	// @Pattern(regexp = "\"^[a-zA-Z0-9_]*$\"")
	@NotNull
	private String password;
	//private  Message email;
	//@NotNull
	private Date birthdays;
	
	
	public Date getBirthdays() {
		return birthdays;
	}

	public void setBirthdays(Date birthdays) {
		this.birthdays = birthdays;
	}

//	PUBLIC MESSAGE GETEMAIL() {
//		RETURN EMAIL;
//	}
//
//	PUBLIC VOID SETEMAIL(MESSAGE EMAIL) {
//		THIS.EMAIL = EMAIL;
//	}
	private boolean isAdmin = false;
	
	@OneToMany(mappedBy ="user", fetch = FetchType.LAZY)
	private Set<Order>order;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private Set<Note> notes;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	  private Set<Viewing> views;


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Set<Order> getOrder() {
		return order;
	}

	public void setOrder(Set<Order> order) {
		this.order = order;
	}

	public Set<Note> getNotes() {
		return notes;
	}

	public void setNotes(Set<Note> notes) {
		this.notes = notes;
	}

	public Set<Viewing> getViews() {
		return views;
	}

	public void setViews(Set<Viewing> views) {
		this.views = views;
	}

	

}
