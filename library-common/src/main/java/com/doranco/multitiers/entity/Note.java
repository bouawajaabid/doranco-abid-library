package com.doranco.multitiers.entity;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name="l_note")

@IdClass(IdNote.class)
public class Note  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public Note() {
		
	}


 
	@Id
   @ManyToOne
   @NotNull
    private Book book;
	
	@Id
	@ManyToOne 
	@NotNull
	private User user;
	
	@NotNull
	private Date noteDate;
	@NotNull
	@Min(0)
	@Max(5)
	private int value;
	@Size(min =0, max =240)
	private String Comment;
	
	
	public Date getNoteDate() {
		return noteDate;
	}

	public void setNoteDate(Date noteDate) {
		this.noteDate = noteDate;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getComment() {
		return Comment;
	}



	public void setComment(String comment) {
		Comment = comment;
	}

	
	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}


}
