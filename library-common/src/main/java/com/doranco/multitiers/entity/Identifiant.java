package com.doranco.multitiers.entity;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Identifiant implements Serializable {

	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private long id;

	

	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Identifiant [id=" + id + "]";
	}

	public void setId(int id) {
		this.id = id;
	}

	public Identifiant() {

	}
}
