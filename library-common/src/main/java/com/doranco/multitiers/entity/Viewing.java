package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;


@Entity
@Table(name ="l_viewing")
@IdClass(IdView.class)
public class Viewing implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private IdView idView = new IdView();
	
	private Date startDate;
	
	
	// nb heures
	@Min(1)
	@Max(4)
	private long duration;

	@Id
	@ManyToOne
	private Book book;
	 @Id
	 @ManyToOne
	 private User user;

	
	public Book getBook() {
		return book;
	}


	public void setBook(Book book) {
		this.book = book;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}




	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public long getDuration() {
		return duration;
	}


	public void setDuration(long duration) {
		this.duration = duration;
	}

	public IdView getIdView() {
		return idView;
	}

	public void setIdView(IdView idView) {
		this.idView = idView;
	}
	
	
}
