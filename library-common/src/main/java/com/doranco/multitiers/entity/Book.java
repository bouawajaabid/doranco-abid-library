package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name="l_book")
public class Book extends Identifiant implements Serializable {

	/**
	 *  
	 */
	private static final long serialVersionUID = 1L;


    @NotNull
    @Pattern(regexp="^\\d{9}[\\d|X]$") 
    // le format de ISBN doit respecte cette forme  123412341X
	private String ISBN;
    @NotNull
	private String title;

	@OneToMany(mappedBy="book", fetch = FetchType.LAZY)
 private Set<Note>notes;
	
	@OneToMany(mappedBy="book", fetch = FetchType.LAZY)
	private  Set<Viewing> viewing;
	
 
	public Set<Viewing> getViews() {
		return viewing;
	}

	public void setViews(Set<Viewing> views) {
		this.viewing = views;
	}

	public Set<Note> getNotes() {
		return notes;
	}

	public void setNotes(Set<Note> notes) {
		this.notes = notes;
	}

	public String getISBN() {
		return ISBN;
	}

	public Book() {

	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	
	@Override
	public String toString() {
		return "Book [id="  + ", ISBN=" + ISBN + ", title=" + title + "]";
	}

}
