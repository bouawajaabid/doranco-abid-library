package com.doranco.multitiers.ejb.interfaces;

import java.util.List;

import com.doranco.multitiers.entity.Book;
import com.doranco.multitiers.exceptions.LibraryException;
import com.dornaco.multitiers.util.Page;

public interface IBook {

	List<Book> findBook(String title) throws LibraryException;

	public Page<Book> find(int pageSize, int pageNumber) throws LibraryException;

	public Book getBook(long id)throws LibraryException;

	public void createBook(Book book)throws LibraryException;

}
