package com.doranco.multitiers.ejb.interfaces;

import java.util.List;

import javax.ejb.Remote;

import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.exceptions.UserNotFoundException;
import com.dornaco.multitiers.util.Page;

@Remote
public interface IUser {

	public User suscribe(User user) throws LibraryException;

	public String connect(String userName, String password, String uri) throws LibraryException;

	public List<User> findAllUser() throws LibraryException;

	public User getUser(long id) throws LibraryException;

	public Page<User> find(int pageSize, int pageNumber) throws LibraryException;

	public void createUser(User vmToEntities) throws LibraryException;

}
