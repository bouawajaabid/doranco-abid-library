package com.doranco.multitiers.ejb.interfaces;

import com.doranco.multitiers.entity.Note;
import com.doranco.multitiers.exceptions.LibraryException;

public interface INote {

	public void createNote(Note note) throws LibraryException;

}
